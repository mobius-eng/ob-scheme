# README #

`ob-scheme.el` for Emacs Orgmode replaces Guile Scheme support with MIT-Scheme support.

### Setting up ###

* Add `(require 'xscheme)` into `.emacs` file if it is not there yet
* Delete `ob-scheme.el` and `ob-scheme.elc` from `~/.emacs.d/elpa/org-*` and `~/.emacs.d/elpa/org-plus-contrib-*`
* Put provided `ob-scheme.el` in both locations.
* Enjoy!

More info: https://mobiusengineering.wordpress.com/2015/01/11/using-emacs-org-with-mit-scheme/